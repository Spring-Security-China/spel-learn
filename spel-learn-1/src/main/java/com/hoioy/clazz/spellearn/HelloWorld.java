package com.hoioy.clazz.spellearn;

import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

public class HelloWorld {
    public static void main(String[] args) {
        two();
    }

    //2. 控制表达式返回结果
    public static void two() {
        //查看getValue方法         //  desiredResultType
        //通过表达式本身隐士描述了计算结果
        //  我们可以手动指定计算结果
        ExpressionParser parser = new SpelExpressionParser();

        //强制转换
        String r = (String) parser.parseExpression("'1'").getValue();
        System.out.println(r);

        //使用desiredResultType
        r = parser.parseExpression("'1'").getValue(String.class);
        System.out.println(r);

        //使用desiredResultType
        Integer i = parser.parseExpression("'1'").getValue(Integer.class);
        System.out.println(i);

        Foo foo = parser.parseExpression("new com.hoioy.clazz.spellearn.Foo('name')").getValue(Foo.class);
        System.out.println(foo.getName());

        //报错: 如果转化失败会报错
        //i = parser.parseExpression("'a'").getValue(Integer.class);
        //System.out.println(i);
    }

    //1. 简单编写表达式
    public static void one() {
        //写一些简单表达式
        ExpressionParser parser = new SpelExpressionParser();
        //可以调用函数的方法
        String str = "'Hello World'.concat('!')";
        System.out.println(parser.parseExpression(str).getValue());

        // 表达式定义,使用双引号也可以，但是需要转义,不建议使用
        str = "(\"Hello \" + \"World !!!\").substring(6,12)";
        // 与使用单引号一致
        str = "'Hello World !!!'.substring(6,12)";
        System.out.println(parser.parseExpression(str).getValue());

        //尝试字符串类的其他方法

        str = "'Hello World !!!'.substring(6,12)";
        // 表达式语言一句话，自由组合
        str = "'Hello World 1 !!!'.indexOf('1') + 'Hello World !!!'.indexOf('1') ";
        // 其他类型
        str = "1+2+3";
        System.out.println(parser.parseExpression(str).getValue());

        //可以直接调用构造函数
        System.out.println(parser.parseExpression("new String('hello world').toUpperCase()").getValue());
        //可以调用属性
        System.out.println(parser.parseExpression("'str'.bytes").getValue());
        //嵌套属性
        System.out.println(parser.parseExpression("'str'.bytes.length").getValue());

        //现在，我们可以使用String，Int等等类型，那么可以使用自定义的类吗？
        //新建一个Foo类
        //报错
//        System.out.println(parser.parseExpression("new Foo('name').getName()").getValue());
        //得用全路径
        System.out.println(parser.parseExpression("new com.hoioy.clazz.spellearn.Foo('name').getName()").getValue());
        //如果Foo的name属性，有对应的Getter，或者为public修饰，那么可以直接度取属性
        System.out.println(parser.parseExpression("new com.hoioy.clazz.spellearn.Foo('name').name").getValue());

        //表达式语言语法非常灵活，不一一介绍，下一章详细介绍
    }
}


