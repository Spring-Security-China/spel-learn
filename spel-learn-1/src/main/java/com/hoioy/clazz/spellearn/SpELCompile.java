//package com.hoioy.clazz.spellearn;
//
//import com.hoioy.clazz.spellearn.model.Inventor;
//import com.hoioy.clazz.spellearn.model.PlaceOfBirth;
//import com.hoioy.clazz.spellearn.model.Society;
//import org.springframework.expression.Expression;
//import org.springframework.expression.spel.SpelCompilerMode;
//import org.springframework.expression.spel.SpelParserConfiguration;
//import org.springframework.expression.spel.standard.SpelExpressionParser;
//
//import java.util.Arrays;
//import java.util.Date;
//
//public class SpELCompile {
//    public static void main(String[] args) {
//        //默认情况下， SpEL 表达式只有在求值时才会进行表达式计算，
//        // 所以表达式可以在运行时进行动态修改。但如果一个表达式被重复调用的次数很多，
//        // 那么就必须使用 SpelCompiler 编译器来保证性能。
//
//        // SpelCompiler 编译器会将表达式编译成字节码，只有在运行时表达式发生变化时，才会被重新编译。
//
//        // SpelCompiler 编译器适用于被调用的频率较高且表达式不经常发生变化的场景
//
//
//        //默认的编译表达式是off：SpelParserConfiguration.defaultCompilerMode
//
//
//        //我们通过指定编译模式和类加载器来新建 Spel 解析配置器。
//        SpelParserConfiguration config = new SpelParserConfiguration(SpelCompilerMode.IMMEDIATE, ClassLoader.getSystemClassLoader());
//        SpelExpressionParser parser = new SpelExpressionParser(config);
//        Expression exp = parser.parseExpression("'Hello World'");
//        String message = (String) exp.getValue();
//        System.out.println(message);
//
//        //上面示例看不出编译模式的优势
//        //下面尝试计算执行时间
//        test();
//    }
//
//
//    public static void test() {
//        // 构建模型对象，方便读取
//        Inventor hai = new Inventor("海先生", new Date(), "中国");
//        hai.setPlaceOfBirth(new PlaceOfBirth("北京", "中国"));
//        hai.setInventions(new String[]{"i1", "i2", "i3"});
//
//        Inventor hainv = new Inventor("海女士", new Date(), "中国");
//        hainv.setPlaceOfBirth(new PlaceOfBirth("北京", "中国"));
//        hainv.setInventions(new String[]{"in1", "in2", "in3", "in4"});
//
//        Society society = new Society();
//        society.setName("码闻创造营");
//        society.getMembers().addAll(Arrays.asList(hai, hainv));
//        society.getOfficers().put("advisors", Arrays.asList(hai, hainv));
//        society.getOfficers().put("president", hainv);
//
//
//        SpelExpressionParser parser = new SpelExpressionParser(new SpelParserConfiguration(SpelCompilerMode.OFF,null));
////        SpelExpressionParser parser = new SpelExpressionParser();
//
////        System.out.println(parser.parseExpression("members[1].Inventions[3]").getValue(society, String.class));
//
//        for (int i = 0; i < 10; i++) {
//            aa(society, parser);
//        }
//    }
//
//    public static void aa(Society society, SpelExpressionParser parser) {
//        long before = System.currentTimeMillis();
//        for (int i = 0; i < 50000; i++) {
//            parser.parseExpression("members[1].Inventions[3]").getValue(society);
//        }
//        long end = System.currentTimeMillis();
//        System.out.println("耗费时间：" + (end - before));
//    }
//}
//
//
