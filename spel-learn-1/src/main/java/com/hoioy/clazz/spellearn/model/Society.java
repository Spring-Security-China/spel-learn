package com.hoioy.clazz.spellearn.model;

import java.util.*;

//社团
public class Society {
    //社团名称
    private String name;
    //成员列表
    private List<Inventor> members = new ArrayList<Inventor>();
    //官员
    private Map officers = new HashMap();

    public Society(){}

    public List getMembers() {
        return members;
    }

    public Map getOfficers() {
        return officers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMember(String name) {
        for (Inventor inventor : members) {
            if (inventor.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }
}
