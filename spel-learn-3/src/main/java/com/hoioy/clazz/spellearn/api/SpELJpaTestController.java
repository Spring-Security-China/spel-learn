package com.hoioy.clazz.spellearn.api;

import com.hoioy.clazz.spellearn.entity.UserInfo;
import com.hoioy.clazz.spellearn.repository.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.Random;

@RestController
public class SpELJpaTestController {

    @Autowired
    private UserInfoRepository userInfoRepository;

    //初始化数据
    @PostConstruct
    private void init() {
        UserInfo userInfo = new UserInfo();
        userInfo.setName("a1");
        userInfoRepository.save(userInfo);
    }

    @GetMapping("/add")
    public UserInfo user() {
        UserInfo userInfo = new UserInfo();
        userInfo.setName("test" + new Random().nextInt());
        return userInfoRepository.save(userInfo);
    }


    @GetMapping("/q")
    public UserInfo q() {
        return userInfoRepository.findAll().get(0);
    }


    @GetMapping("/q1")
    public Object q1(@RequestParam("name") String name) {
        return userInfoRepository.q1(name);
    }

    @GetMapping("/q2")
    public Object q2(@RequestParam("name") String name) {
        return userInfoRepository.q2(name);
    }

    @GetMapping("/q3")
    public Object q3(@RequestParam("name") String name) {
        UserInfo userInfo = new UserInfo();
        userInfo.setName(name);
        return userInfoRepository.q3(userInfo);
    }

}
