package com.hoioy.clazz.spellearn.conifg;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class DynamicAuthorization {

    public Boolean dynamicAccess(HttpServletRequest request, Authentication authentication) {
        //TODO 动态鉴权逻辑
        //设置为 false 则报错
        return false;
    }

}
