package com.hoioy.clazz.spellearn;

import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

public class HelloWorldWrite {
    public static void main(String[] args) {
        //从I/O角度来说，程序要么就是读要么就是写
        // 与普通运行方法和程序一样，SpEL 当然也可以执行写操作
        // setValue
        // isWritable

        Foo foo = new Foo("foo");

        String str = "name" ;
        ExpressionParser parser = new SpelExpressionParser();
        Expression exp = parser.parseExpression(str);

        // 区别来了
        exp.setValue(foo,"foo1");
        System.out.println(foo.getName());
    }
}
