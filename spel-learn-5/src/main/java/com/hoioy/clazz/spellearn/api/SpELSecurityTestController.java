package com.hoioy.clazz.spellearn.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpELSecurityTestController {

    @GetMapping("/q")
    public String q() {
        return "q:已经拿到数据了，说明走了动态鉴权逻辑";
    }

}
