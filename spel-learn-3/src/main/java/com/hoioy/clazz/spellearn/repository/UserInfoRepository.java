package com.hoioy.clazz.spellearn.repository;

import com.hoioy.clazz.spellearn.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo, Integer> {
    //使用索引
    @Query("select u from UserInfo u where u.name = ?#{[0]}")
    List<UserInfo> q1(String name);

    //表示entityName
    //根据指定的Repository，自动计算出了entityName
    @Query("select u from #{#entityName} u where u.name = ?1")
    List<UserInfo> q2(String name);

    //使用:#应用对象
    @Query("select u from UserInfo u where u.name = :#{#userInfo.name}")
    List<UserInfo> q3(@Param("userInfo") UserInfo userInfo);
}
