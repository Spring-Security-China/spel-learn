package com.hoioy.clazz.spellearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpelLearnApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpelLearnApplication.class, args);
    }

}
