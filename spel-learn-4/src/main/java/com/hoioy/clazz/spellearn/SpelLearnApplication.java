package com.hoioy.clazz.spellearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class SpelLearnApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpelLearnApplication.class, args);
    }

}
