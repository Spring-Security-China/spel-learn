package com.hoioy.clazz.spellearn.api;

import com.hoioy.clazz.spellearn.entity.UserInfo;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@RestController
@CacheConfig(cacheNames = "user")
public class SpELCacheTestController {
    List<UserInfo> userInfos = new ArrayList<>();

    //初始化数据
    @PostConstruct
    private void init() {
        userInfos.add(new UserInfo("a1"));
        userInfos.add(new UserInfo("a2"));
        userInfos.add(new UserInfo("a3"));
    }

    @GetMapping("/add")
    public Boolean user(@RequestParam("n") String n) {
        return userInfos.add(new UserInfo(n));
    }

    // 通常清空下，直接使用 SpEL 表达式来指定缓存项的 Key 比自定义一个 KeyGenerator 更为简单。
    @GetMapping("/q")
    @Cacheable(key = "#i")
    public UserInfo q(@RequestParam("i") Integer i) {
        System.out.println("q,从数据库中读取");
        return userInfos.get(i);
    }

    // condition使用示例
    //Cache 注解的 key、unless、condition 等都是支持 SpEL 的
    @GetMapping("/qc")
//    @Cacheable(key = "#i", condition = "#i < 2")
    //如果key都为#i那么与/q接口共享了key，为了区别开而更改key值
    @Cacheable(key = "'qc'+#i", condition = "#i < 2")
    public UserInfo qc(@RequestParam("i") Integer i) {
        System.out.println("qc,从数据库中读取");
        return userInfos.get(i);
    }

    //不使用缓存
    @GetMapping("/qn")
    public UserInfo qn(@RequestParam("i") Integer i) {
        System.out.println("qn,从数据库中读取");
        return userInfos.get(i);
    }
}
