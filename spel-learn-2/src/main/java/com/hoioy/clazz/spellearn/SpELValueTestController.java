package com.hoioy.clazz.spellearn;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

//@RestController
@RestController("aa")
public class SpELValueTestController {

    //自定义配置,非SPEL
    @Value("${spel-learn.test}")
    private Object v0;

    // $符号引用环境变量，注意$符号不是表达式了
    // @Value("#{}")   SpEL表达式
    // @Value("${}") 可以获取对应属性文件中定义的属性值
    // environment variable。OS level
    @Value("${path}")
    private Object v5;
    // @Value("${classpath}")
//    private Object v22;
    @Value("${java_home}")
    private Object v6;

    @Value("#{ 'HelloWorld' }")
    private String v1;

    @Value("#{ T(java.lang.Math).random() * 100.0 }")
    private String v2;

    // #{}的原理，表达式模板  1. 原理：StandardBeanExpressionResolver

    //spring boot上下文内置了很多变量
    //获取Java property. JVM argument
//    @Value("#{ systemProperties}")
//    private Map v3;
    @Value("#{systemProperties['sun.desktop'] }")
    private String v3;

    //上下文中自动有很多Bean
    //全路径还是只是名称？全路径是不行的，会将“点儿”符号自动识别为获取属性，因此会报错：
    //   “org.springframework.expression.spel.SpelEvaluationException: EL1008E: Property or field 'spring' cannot be found
    //   on object of type 'org.springframework.beans.factory.config.BeanExpressionContext' ”
//    @Value("#{ spring.mvc-org.springframework.boot.autoconfigure.web.servlet.WebMvcProperties }")
//    private Map v4;

//    @Value("#{ spELValueTestController }")
//    private Object v4;

    //大写引用Bean是不行的
//    @Value("#{ SpELValueTestController }")
//    private Object v4;
    //如果手动修改bean名称，则配置此bean名称
    @Value("#{ aa }")
    private Object v4;

    @GetMapping("/value")
    public Object valueMap() {
        return Arrays.asList(
                v1,
                v2,
                v3,
                v4,
                v5,
                v6,
                v0
        );
    }
}
